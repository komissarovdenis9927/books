package com.computools.web.controller;

import com.computools.service.dto.AuthorDto;
import com.computools.audit.entities.Author;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import com.computools.service.service.AuthorService;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

@RestController
@RequestMapping("/library/authors")
public class AuthorController {


    private AuthorService authorService;

    public AuthorController(AuthorService authorService){
        this.authorService = authorService;
    }

    @PostMapping()
    @PreAuthorize("hasRole('ADMIN')")
    public ResponseEntity createAuthor(@Valid @RequestBody AuthorDto authorDto){

        return new ResponseEntity(authorService.createAuthor(authorDto), HttpStatus.OK);
    }

    @PutMapping("/{id}")
    @PreAuthorize("hasRole('ADMIN')")
    public ResponseEntity updateAuthor(@RequestBody AuthorDto authorDto, @PathVariable int id){

        return ResponseEntity.ok(authorService.updateAuthor(authorDto, id));

    }

    @DeleteMapping("/{id}")
    @PreAuthorize("hasRole('ADMIN')")
    public ResponseEntity deleteAuthor(@PathVariable int id){
        authorService.deleteAuthor(id);
        return new ResponseEntity(HttpStatus.NO_CONTENT);
    }

    @GetMapping("/{id}")
    public ResponseEntity getAuthor(@PathVariable int id){
        AuthorDto authorDto = authorService.getAuthor(id);
        return new ResponseEntity(authorDto, HttpStatus.OK);
    }

    @GetMapping("/list")
    public ResponseEntity getAuthorsList(@NotNull final Pageable pageable){

        Page<Author> authors = authorService.findPaginated(pageable);


        return new ResponseEntity(authors, HttpStatus.OK);
    }
}
