package com.computools.web.controller;


import com.computools.service.dto.BookDto;
import com.computools.audit.entities.Book;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import com.computools.service.service.BookService;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

@RestController
@RequestMapping("/library/books")
public class BookController {

    @Autowired
    BookService bookService;

    @PostMapping()
    @PreAuthorize("hasRole('ADMIN')")
    public ResponseEntity createBook(@Valid @RequestBody BookDto bookDto) throws Exception {

        bookDto = bookService.createBook(bookDto);
        return new ResponseEntity(bookDto, HttpStatus.OK);
    }

    @DeleteMapping("/{id}")
    @PreAuthorize("hasRole('ADMIN')")
    public ResponseEntity deleteBook(@PathVariable int id) throws Exception {

        bookService.deleteBook(id);
        return new ResponseEntity(HttpStatus.NO_CONTENT);
    }

    @PutMapping("/{id}")
    @PreAuthorize("hasRole('ADMIN')")
    public ResponseEntity updateBook(@RequestBody BookDto bookDto, @PathVariable int id) throws Exception {

        bookDto = bookService.updateBook(bookDto, id);
        return new ResponseEntity(bookDto, HttpStatus.OK);
    }

    @GetMapping("/{id}")
    public ResponseEntity getBook(@PathVariable int id) throws Exception {

        BookDto bookDto = bookService.getBook(id);
        return new ResponseEntity(bookDto, HttpStatus.OK);
    }

    @GetMapping("/list")
    public ResponseEntity getBookList(@NotNull Pageable pageable ){

        Page<Book> bookPage = bookService.findPaginated(pageable);

        return new ResponseEntity(bookPage, HttpStatus.OK);
    }

}
