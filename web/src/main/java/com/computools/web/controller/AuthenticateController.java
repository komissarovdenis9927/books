package com.computools.web.controller;


import com.computools.service.dto.LoginDto;
import com.computools.service.dto.SignUpDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.computools.service.responce.ApiResponse;
import com.computools.service.responce.JwtAuthenticationResponse;
import com.computools.service.service.AuthenticateService;

import javax.validation.Valid;
import java.io.UnsupportedEncodingException;

@RestController
@RequestMapping("/library/auth")
public class AuthenticateController {

    private AuthenticateService authenticateService;

    @Autowired
    public AuthenticateController(AuthenticateService authenticateService){
        this.authenticateService = authenticateService;
    }

    @PostMapping("/signin")
    public ResponseEntity<JwtAuthenticationResponse> authenticateUser(@Valid @RequestBody LoginDto loginDto) throws UnsupportedEncodingException {

            return authenticateService.authenticateUser(loginDto);

    }

    @PostMapping("/signup")
    public ResponseEntity<ApiResponse> registerUser(@Valid @RequestBody SignUpDto signUpDTO) {
        return  authenticateService.registerUser(signUpDTO);
    }



}
