package com.computools.audit.entities.enums;

public enum UserRoleEnum {
    ADMIN,
    USER,
    ANONYMOUS;

    UserRoleEnum(){

    }
}
