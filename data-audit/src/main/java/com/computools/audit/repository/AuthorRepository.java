package com.computools.audit.repository;


import com.computools.audit.entities.Author;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AuthorRepository extends JpaRepository<Author, Integer> {

    void deleteAuthorById(int id);

}
