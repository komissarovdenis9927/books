package com.computools.audit.repository;


import com.computools.audit.entities.Book;
import org.springframework.data.jpa.repository.JpaRepository;

public interface BookRepository extends JpaRepository<Book, Integer> {
    boolean deleteBookById(int id);
}
