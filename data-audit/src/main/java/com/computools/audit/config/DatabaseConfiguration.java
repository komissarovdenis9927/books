package com.computools.audit.config;

import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@Configuration
@EnableJpaRepositories(basePackages = {"com.computools.audit.repository"})
@EntityScan(value = "com/computools/audit/entities")
public class DatabaseConfiguration {
}
