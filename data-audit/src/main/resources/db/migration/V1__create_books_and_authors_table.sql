CREATE TABLE if not exists authors(
id serial primary key not null,
first_name varchar(255),
last_name varchar(255)
);

CREATE TABLE if not exists books(
id serial primary key not null,
book_name varchar(255),
author integer,
foreign key (author) references authors(id)
);


-- CREATE TABLE if not exists authors(
-- id INTEGER NOT NULL PRIMARY KEY AUTO_INCREMENT,
-- first_name varchar(255),
-- last_name varchar(255)
-- );
--
-- CREATE TABLE if not exists books(
-- id INTEGER NOT NULL PRIMARY KEY AUTO_INCREMENT,
-- book_name varchar(255),
-- author_id integer,
-- foreign key (author_id) references authors(id)
-- );



