CREATE TABLE if not exists users(
  id SERIAL PRIMARY KEY ,
  created_time       timestamp  not null ,
  updated_time       timestamp  not null ,
  name           VARCHAR (255),
  password           VARCHAR (255),
  email              VARCHAR (255) unique,
  username           VARCHAR (255),
  roles              VARCHAR (355)
);





-- CREATE TABLE if not exists users(
--   id                 VARCHAR (36) NOT NULL primary key ,
--   created_time       DATETIME  not null ,
--   updated_time       DATETIME  not null ,
--   firstname           VARCHAR (255),
--   password           VARCHAR (255),
--   email              VARCHAR (255) unique,
--   username           VARCHAR (255),
--   roles              VARCHAR (355)
-- );
--
-- CREATE TABLE if not exists books(
-- id INTEGER NOT NULL PRIMARY KEY AUTO_INCREMENT,
-- book_name varchar(255),
-- author_id integer,
-- foreign key (author_id) references authors(id)
-- );



-- CREATE TABLE if not exists authors(
-- id serial primary key not null,
-- first_name varchar(255),
-- last_name varchar(255)
-- );
--
-- CREATE TABLE if not exists books(
-- id serial primary key not null,
-- book_name varchar(255),
-- author integer,
-- foreign key (author) references authors(id)
-- );
