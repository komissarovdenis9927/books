package com.computools.service.service;

import com.computools.service.dto.AuthorDto;
import com.computools.audit.entities.Author;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface AuthorService {

    AuthorDto createAuthor(AuthorDto authorDto);
    AuthorDto updateAuthor(AuthorDto authorDto, int id);
    void deleteAuthor(int id);
    AuthorDto getAuthor(int id);
    Page<Author> findPaginated(Pageable pageable );


}
