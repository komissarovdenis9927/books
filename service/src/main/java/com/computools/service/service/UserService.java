package com.computools.service.service;

import com.computools.audit.entities.User;

public interface UserService {
    User getUser(String login);
}
