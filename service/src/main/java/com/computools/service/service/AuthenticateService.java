package com.computools.service.service;

import com.computools.service.dto.LoginDto;
import com.computools.service.dto.SignUpDto;
import com.computools.service.dto.UserDto;
import org.springframework.http.ResponseEntity;
import com.computools.service.responce.ApiResponse;
import com.computools.service.responce.JwtAuthenticationResponse;

import java.io.UnsupportedEncodingException;

public interface AuthenticateService {

    void addUser(UserDto userDto);
    ResponseEntity<JwtAuthenticationResponse> authenticateUser(LoginDto loginDto) throws UnsupportedEncodingException;
    ResponseEntity<ApiResponse> registerUser(SignUpDto signUpDto);

}
