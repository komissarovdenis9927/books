package com.computools.service.config;

import com.computools.audit.config.DatabaseConfiguration;
import com.computools.security.jwt.config.SecurityConfig;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

@Configuration
@Import({DatabaseConfiguration.class, SecurityConfig.class})
@ComponentScan(basePackages = "com.computools.service")
public class ServiceConfig {
}
